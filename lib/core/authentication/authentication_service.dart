import 'dart:async';

import '../failure/failure.dart';
import 'authentication_repository.dart';
import 'syntaxis_user.dart';

/// Service for managing user authentication
class AuthenticationService {
  final AuthenticationRepository _authRepository;

  /// Create a new instance of [AuthenticationService] or get the current
  /// instance of the service.
  factory AuthenticationService({
    required AuthenticationRepository authenticationRepository,
  }) {
    _instance ??= AuthenticationService._(
        authenticationRepository: authenticationRepository);
    return _instance!;
  }

  AuthenticationService._(
      {required AuthenticationRepository authenticationRepository})
      : _authRepository = authenticationRepository {
    // initialize the current user, if possible.
    // TODO REMove
    unawaited(initialize());
  }

  /// The currently available instance of the [AuthenticationService].
  static AuthenticationService? _instance;

  /// Contains the current signed in user, if signed in.
  SyntaxisUser? _currentUser;

  /// Returns the currently signed in user, if applicable.
  SyntaxisUser? get currentUser => _currentUser;

  ///
  Stream<SyntaxisUser?> get currentUserStream =>
      _currentUserStreamController.stream;

  final StreamController<SyntaxisUser?> _currentUserStreamController =
      StreamController<SyntaxisUser?>.broadcast();

  /// Sing in with email and password.
  Future<(SyntaxisUser?, Failure?)> login(String email, String password) async {
    try {
      final SyntaxisUser? user = await _authRepository.login(email, password);
      if (user != null) {
        _currentUser = user;
        _currentUserStreamController.add(user);
        return (user, null);
      }
      return (null, Failure(message: 'Failed to login w/o error'));
    } catch (e) {
      return (null, Failure(message: e.toString()));
    }
  }

  /// Dispose the class
  Future<void> dispose() async {
    await _currentUserStreamController.close();
    _instance = null;
  }

  Future<void> initialize() async  {
    final SyntaxisUser? optionalUser = await _authRepository.loginFromToken();
    if (optionalUser != null) {
      _currentUser = optionalUser;
      _currentUserStreamController.add(optionalUser);
    }
  }
}
