import 'package:freezed_annotation/freezed_annotation.dart';

part 'syntaxis_user.freezed.dart';

@freezed
class SyntaxisUser with _$SyntaxisUser {
  const factory  SyntaxisUser({
    required String token,
    required String fullName,
    required int balance,
    required String barcode,
}) =_SyntaxisUser;
}