import 'syntaxis_user.dart';

abstract class AuthenticationRepository {
  Future<SyntaxisUser?> login(String email, String password);

  Future<SyntaxisUser?> loginFromToken();
}