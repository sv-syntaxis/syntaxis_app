// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'syntaxis_user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SyntaxisUser {
  String get token => throw _privateConstructorUsedError;
  String get fullName => throw _privateConstructorUsedError;
  int get balance => throw _privateConstructorUsedError;
  String get barcode => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SyntaxisUserCopyWith<SyntaxisUser> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SyntaxisUserCopyWith<$Res> {
  factory $SyntaxisUserCopyWith(
          SyntaxisUser value, $Res Function(SyntaxisUser) then) =
      _$SyntaxisUserCopyWithImpl<$Res, SyntaxisUser>;
  @useResult
  $Res call({String token, String fullName, int balance, String barcode});
}

/// @nodoc
class _$SyntaxisUserCopyWithImpl<$Res, $Val extends SyntaxisUser>
    implements $SyntaxisUserCopyWith<$Res> {
  _$SyntaxisUserCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? token = null,
    Object? fullName = null,
    Object? balance = null,
    Object? barcode = null,
  }) {
    return _then(_value.copyWith(
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      balance: null == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as int,
      barcode: null == barcode
          ? _value.barcode
          : barcode // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SyntaxisUserImplCopyWith<$Res>
    implements $SyntaxisUserCopyWith<$Res> {
  factory _$$SyntaxisUserImplCopyWith(
          _$SyntaxisUserImpl value, $Res Function(_$SyntaxisUserImpl) then) =
      __$$SyntaxisUserImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String token, String fullName, int balance, String barcode});
}

/// @nodoc
class __$$SyntaxisUserImplCopyWithImpl<$Res>
    extends _$SyntaxisUserCopyWithImpl<$Res, _$SyntaxisUserImpl>
    implements _$$SyntaxisUserImplCopyWith<$Res> {
  __$$SyntaxisUserImplCopyWithImpl(
      _$SyntaxisUserImpl _value, $Res Function(_$SyntaxisUserImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? token = null,
    Object? fullName = null,
    Object? balance = null,
    Object? barcode = null,
  }) {
    return _then(_$SyntaxisUserImpl(
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      balance: null == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as int,
      barcode: null == barcode
          ? _value.barcode
          : barcode // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SyntaxisUserImpl implements _SyntaxisUser {
  const _$SyntaxisUserImpl(
      {required this.token,
      required this.fullName,
      required this.balance,
      required this.barcode});

  @override
  final String token;
  @override
  final String fullName;
  @override
  final int balance;
  @override
  final String barcode;

  @override
  String toString() {
    return 'SyntaxisUser(token: $token, fullName: $fullName, balance: $balance, barcode: $barcode)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SyntaxisUserImpl &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.balance, balance) || other.balance == balance) &&
            (identical(other.barcode, barcode) || other.barcode == barcode));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, token, fullName, balance, barcode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SyntaxisUserImplCopyWith<_$SyntaxisUserImpl> get copyWith =>
      __$$SyntaxisUserImplCopyWithImpl<_$SyntaxisUserImpl>(this, _$identity);
}

abstract class _SyntaxisUser implements SyntaxisUser {
  const factory _SyntaxisUser(
      {required final String token,
      required final String fullName,
      required final int balance,
      required final String barcode}) = _$SyntaxisUserImpl;

  @override
  String get token;
  @override
  String get fullName;
  @override
  int get balance;
  @override
  String get barcode;
  @override
  @JsonKey(ignore: true)
  _$$SyntaxisUserImplCopyWith<_$SyntaxisUserImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
