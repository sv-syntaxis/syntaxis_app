import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../authentication/cubit/auth_cubit.dart';
import '../navigation/navigation.dart';

/// The start point of the Widget tree.
class App extends StatelessWidget {
  /// Creates a new [App] widget.
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: RootNavigator.router(context.read<AuthCubit>()),
    );
  }
}
