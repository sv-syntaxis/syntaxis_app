import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';

import '../authentication/authentication_page.dart';
import '../authentication/cubit/auth_cubit.dart';
import '../home/home_page.dart';

/// Navigation implementation for handling all `/*` routes in the application.
class RootNavigator {
  /// The base router for the application.
  static GoRouter router(AuthCubit authCubit) => GoRouter(
          routes: <RouteBase>[
            GoRoute(
              path: '/login',
              name: AuthenticationPage.routeName,
              builder: (_, __) => const AuthenticationPage(),
            ),
            GoRoute(
              path: '/home',
              name: HomePage.routeName,
              builder: (_, __) => const HomePage(),
            ),
          ],
          initialLocation: '/login',
          refreshListenable: GoRouterRefreshStream(authCubit.stream),
          redirect: (BuildContext context, GoRouterState goRouterState) async {
            final List<String> publicPages = <String>[
              '/login',
            ];
            return await authCubit.state.when(
                loggedOut: (_) {
                  if (publicPages.any((String e) =>
                      goRouterState.uri.toString().contains(e))) {
                    // The user is allowed to be on this page without
                    // authentication.
                    debugPrint('logged out, public page');
                    return null;
                  } else {
                    // User not allowed to be on page, redirect to login.
                    debugPrint('logged out, private page, redirect to login');
                    return '/login';
                  }
                },
                login: () => null,
                loggedIn: () {
                  print('ping');
                  return '/home';
                });
          });
}

/// Temporary helper class to map a [Stream] to a [Listenable].
class GoRouterRefreshStream extends ChangeNotifier {
  /// Constructs a [GoRouterRefreshStream].
  GoRouterRefreshStream(Stream<dynamic> stream) {
    notifyListeners();
    _subscription = stream.asBroadcastStream().listen(
          (dynamic _) => notifyListeners(),
        );
  }

  late final StreamSubscription<dynamic> _subscription;

  @override
  Future<void> dispose() async {
    await _subscription.cancel();
    super.dispose();
  }
}
