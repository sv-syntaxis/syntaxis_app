import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_state.freezed.dart';

/// Class for managing the current state of the applications authentication
/// state.
@freezed
class AuthState with _$AuthState {
  /// State indicating the user is logged out.
  const factory AuthState.loggedOut({String? errorMessage}) = _LoggedOut;
  /// State indicating the application is busy logging in.
  const factory AuthState.login() = _Loading;
  /// State indicating the user is logged in.
  const factory AuthState.loggedIn() = _LoggedIn;
}
