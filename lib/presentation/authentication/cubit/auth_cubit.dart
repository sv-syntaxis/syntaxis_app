import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/authentication/authentication_service.dart';
import '../../../core/authentication/syntaxis_user.dart';
import '../../../core/failure/failure.dart';
import 'auth_state.dart';

/// The Auth cubit is used for handling the current authentication state of the
/// application.
class AuthCubit extends Cubit<AuthState> {
  final AuthenticationService authService;

  late final StreamSubscription<SyntaxisUser?> _userSubscription;

  /// Initializes an [AuthCubit].
  AuthCubit({required this.authService}) : super(const AuthState.loggedOut()) {
    initialize();
  }

  Future<void> initialize() async {
    _userSubscription =
        authService.currentUserStream.listen((SyntaxisUser? user) {
      if (user != null) {
        emit(const AuthState.loggedIn());
      } else {
        emit(const AuthState.loggedOut());
      }
    });
  }

  /// Method used to login a user.
  Future<void> login(String email, String password) async {
    final (SyntaxisUser?, Failure?) response =
        await authService.login(email, password);
    if (response.$1 != null) {
      emit(const AuthState.loggedIn());
    } else {
      emit( AuthState.loggedOut(errorMessage: response.$2!.message));
      // TODO(daniel): add failure message to state.
    }
  }
}
