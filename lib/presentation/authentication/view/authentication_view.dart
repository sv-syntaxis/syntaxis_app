import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

import '../cubit/auth_cubit.dart';

/// Widget containing the view for authenticating a user.
class AuthenticationView extends StatelessWidget {
  /// Defines the formKey for the authentication form.
  final GlobalKey<FormBuilderState> _formKey = GlobalKey();

  /// Constructs an [AuthenticationView]
  AuthenticationView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: AutofillGroup(
            child: FormBuilder(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  FormBuilderTextField(
                    name: 'email',
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.email(
                          errorText: 'This is not a valid email')
                    ]),
                    autofillHints: const <String>[AutofillHints.username],
                    decoration: const InputDecoration(hintText: 'E-mail'),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  FormBuilderTextField(
                    name: 'password',
                    obscureText: true,
                    validator: FormBuilderValidators.required(
                        errorText: 'This field is required'),
                    decoration: const InputDecoration(
                      hintText: 'Password',
                    ),
                    autofillHints: const <String>[AutofillHints.password],
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      TextInput.finishAutofillContext();

                      if (_formKey.currentState!.saveAndValidate()) {
                        await context.read<AuthCubit>().login(
                              _formKey.currentState!.fields['email']!.value
                                  as String,
                              _formKey.currentState!.fields['password']!.value
                                  as String,
                            );
                      }
                    },
                    child: const Text('login'),
                  ),
                  Text(
                    context.watch<AuthCubit>().state.map(
                        loggedOut: (e) => (e.errorMessage ?? '').toString(),
                        login: (_) => 'login',
                        loggedIn: (_) => 'loggedIn'),
                    style: const TextStyle(color: Colors.red),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
