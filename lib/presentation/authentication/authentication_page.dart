import 'package:flutter/material.dart';

import 'view/authentication_view.dart';

/// Page for initializing the [AuthenticationView].
class AuthenticationPage extends StatelessWidget {
  /// The route name used for the goNamed function of GoRouter.
  static const String routeName = 'AuthenticationPage';

  /// Constructs an [AuthenticationPage].
  const AuthenticationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return AuthenticationView();
  }
}
