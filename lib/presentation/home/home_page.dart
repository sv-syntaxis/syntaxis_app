import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../core/authentication/authentication_service.dart';
import '../../data/authentication/authentication_repository_impl.dart';
import 'barcode_cubit.dart';

class HomePage extends StatelessWidget {
  /// The route name used for the goNamed function of GoRouter.
  static const String routeName = 'HomePage';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BarcodeCubit>(
      create: (BuildContext context) => BarcodeCubit(AuthenticationService(
          authenticationRepository: AuthenticationRepositoryImpl())),
      child: Builder(builder: (BuildContext context) {
        return BlocBuilder<BarcodeCubit, BarcodeState>(
          builder: (BuildContext context, BarcodeState state) {
            return DecoratedBox(
              decoration: const BoxDecoration(color: Colors.white),
              child: Padding(
                padding: const EdgeInsets.all(32.0),
                child: BarcodeWidget(
                    data: state.barcode ?? '', barcode: Barcode.code128()),
              ),
            );
          },
        );
      }),
    );
  }
}
