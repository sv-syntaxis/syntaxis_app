part of 'barcode_cubit.dart';

@immutable
class BarcodeState {
  final String? barcode;

  BarcodeState(this.barcode);

}

