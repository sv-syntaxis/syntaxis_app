import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../core/authentication/authentication_service.dart';

part 'barcode_state.dart';

class BarcodeCubit extends Cubit<BarcodeState> {
  final AuthenticationService authenticationService;

  ///
  BarcodeCubit(this.authenticationService)
      : super(BarcodeState(authenticationService.currentUser?.barcode)) {
    unawaited(_listenToUserChanges());
  }

  Future<void> _listenToUserChanges() async {
    authenticationService.currentUserStream.listen((event) {
      if (event != null) {
        emit(BarcodeState(event.barcode));
      } else {
        emit(BarcodeState(null));
      }
    });
  }
}
