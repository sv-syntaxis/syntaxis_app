import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:retrofit/retrofit.dart';

import '../authentication/authentication_request_dto.dart';
import '../authentication/authentication_response.dart';

part 'rest_client.g.dart';


/// A [RestClient] to the Syntaxis backend.
///
/// Uses Retrofit to generate implementations.
@RestApi(baseUrl: 'https://beheer.syntaxis.nl/api')
abstract class RestClient {
  ///
  factory RestClient(Dio dio) => _RestClient(
    dio
      ..interceptors.addAll(<Interceptor>[
        if (!kReleaseMode)
          PrettyDioLogger(
            requestHeader: true,
            requestBody: true,
            responseBody: true,
            responseHeader: true,
            error: true,
            compact: true,
            maxWidth: 90,
          ),
        // AuthInterceptor(),
      ]),
  );

  /// A POST request for authenticating a user
  @POST('/user/signin')
  Future<AuthenticationResponse> postSignIn(@Body() AuthenticationRequestDto requestDto);
}