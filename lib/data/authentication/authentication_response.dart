// ignore_for_file: invalid_annotation_target
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../core/authentication/syntaxis_user.dart';

part 'authentication_response.freezed.dart';
part 'authentication_response.g.dart';

////
@freezed
class AuthenticationResponse with _$AuthenticationResponse {
  ///
  const factory AuthenticationResponse(
          {required bool success,
          required String? generalMessage,
          required String? error,
          required AuthenticationPayloadResponse payload}) =
      _AuthenticationResponse;

  const AuthenticationResponse._();

  ///
  SyntaxisUser get toSyntaxisUser => SyntaxisUser(
        token: payload.token,
        fullName: payload.owner.fullname,
        balance: payload.owner.credit,
        barcode: payload.owner.uid,
      );


  /// Create a new [AuthenticationResponse] from json.
  factory AuthenticationResponse.fromJson(Map<String, dynamic> json) =>
      _$AuthenticationResponseFromJson(json);
}

///
@freezed
class AuthenticationPayloadResponse with _$AuthenticationPayloadResponse {
  ///
  const factory AuthenticationPayloadResponse({
    @JsonKey(name: 'user_id') required int userId,
    required String token,
    @JsonKey(name: 'created_at') required DateTime createdAt,
    required OwnerResponse owner,
  }) = _AuthenticationPayloadResponse;

  /// Create a new [AuthenticationPayloadResponse] from json.
  factory AuthenticationPayloadResponse.fromJson(Map<String, dynamic> json) =>
      _$AuthenticationPayloadResponseFromJson(json);
}

@freezed
class OwnerResponse with _$OwnerResponse {
  ///
  const factory OwnerResponse({
    required int id,
    required String email,
    required String firstname,
    required String insertion,
    required String lastname,
    required String country,
    required String street,
    required String postal,
    required String city,
    required String phone,
    required DateTime dob,
    required String gender,
    required String studentnumber,
    required String? comments,
    required bool contribution,
    required bool transaction,
    required bool sbc,
    @JsonKey(name: 'pas_created') required bool pasCreated,
    @JsonKey(name: 'mandaat_id') required int mandaatId,
    required String uid,
    @JsonKey(name: 'deleted_at') required DateTime? deletedAt,
    @JsonKey(name: 'created_at') required DateTime createdAt,
    @JsonKey(name: 'updated_at') required DateTime? updatedAt,
    @JsonKey(name: 'education_id') required int educationId,
    @JsonKey(name: 'email_newsletters') required bool emailNewsletters,
    @JsonKey(name: 'email_promotions') required bool emailPromotions,
    required int anonimized,
    required int credit,
    required String fullname,
    // required EducationResponse education,
    // required List<Role> roles,
  }) = _OwnerResponse;

  const OwnerResponse._();

  /// Create a new [OwnerResponse] from json.
  factory OwnerResponse.fromJson(Map<String, dynamic> json) =>
      _$OwnerResponseFromJson(json);
}

// {
//      success: true,
//      generalMessage: "Successfully logged in.",
//      error: null,
//      payload: {
//          user_id: 1411,
//          ip: "172.18.0.6",
//          token: "4d9714adfecaa342709f5af05111f3f30f42e1b2",
//          created_at: "2023-09-11 19:27:56",
//          owner: {
//              id: 1411,
//              email: "daniel.roek@gmail.com",
//              firstname: "Daniel",
//              insertion: "",
//              lastname: "Roek",
//              country: "nl",
//              street: "Veldkampstraat 30",
//              postal: "7513ZC",
//              city: "Enschede",
//              phone: "0617551398",
//              dob: "1995-07-19",
//              gender: "m",
//              studentnumber: "447897",
//              comments: null,
//              contribution: true,
//              transaction: true,
//              sbc: true,
//              pas_created: true,
//              mandaat_id: 1402,
//              uid: "EA2JBIFJNU",
//              deleted_at: null,
//              created_at: "2017-08-28 18:38:06",
//              updated_at: "2023-06-26 21:49:38",
//              education_id: 70,
//              email_newsletters: true,
//              email_promotions: true,
//              anonimized: 0,
//              credit: -322,
//              fullname: "Daniel Roek",
//              education: {id: 70, name: HBO-ICT},
//              roles: [
//                 {
//                      id: 3,
//                      name: "Admins",
//                      display_name: "Beheer",
//                      description: "Administrators van Syntaxis",
//                      created_at: "2015-12-12 14:17:08",
//                      updated_at: "2015-12-12 14:17:08",
//                      fee: 1250
//                 }
//              ]
//         }
//     }
// }
