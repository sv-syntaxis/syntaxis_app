// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$AuthenticationResponseImpl _$$AuthenticationResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$AuthenticationResponseImpl(
      success: json['success'] as bool,
      generalMessage: json['generalMessage'] as String?,
      error: json['error'] as String?,
      payload: AuthenticationPayloadResponse.fromJson(
          json['payload'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$AuthenticationResponseImplToJson(
        _$AuthenticationResponseImpl instance) =>
    <String, dynamic>{
      'success': instance.success,
      'generalMessage': instance.generalMessage,
      'error': instance.error,
      'payload': instance.payload,
    };

_$AuthenticationPayloadResponseImpl
    _$$AuthenticationPayloadResponseImplFromJson(Map<String, dynamic> json) =>
        _$AuthenticationPayloadResponseImpl(
          userId: json['user_id'] as int,
          token: json['token'] as String,
          createdAt: DateTime.parse(json['created_at'] as String),
          owner: OwnerResponse.fromJson(json['owner'] as Map<String, dynamic>),
        );

Map<String, dynamic> _$$AuthenticationPayloadResponseImplToJson(
        _$AuthenticationPayloadResponseImpl instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'token': instance.token,
      'created_at': instance.createdAt.toIso8601String(),
      'owner': instance.owner,
    };

_$OwnerResponseImpl _$$OwnerResponseImplFromJson(Map<String, dynamic> json) =>
    _$OwnerResponseImpl(
      id: json['id'] as int,
      email: json['email'] as String,
      firstname: json['firstname'] as String,
      insertion: json['insertion'] as String,
      lastname: json['lastname'] as String,
      country: json['country'] as String,
      street: json['street'] as String,
      postal: json['postal'] as String,
      city: json['city'] as String,
      phone: json['phone'] as String,
      dob: DateTime.parse(json['dob'] as String),
      gender: json['gender'] as String,
      studentnumber: json['studentnumber'] as String,
      comments: json['comments'] as String?,
      contribution: json['contribution'] as bool,
      transaction: json['transaction'] as bool,
      sbc: json['sbc'] as bool,
      pasCreated: json['pas_created'] as bool,
      mandaatId: json['mandaat_id'] as int,
      uid: json['uid'] as String,
      deletedAt: json['deleted_at'] == null
          ? null
          : DateTime.parse(json['deleted_at'] as String),
      createdAt: DateTime.parse(json['created_at'] as String),
      updatedAt: json['updated_at'] == null
          ? null
          : DateTime.parse(json['updated_at'] as String),
      educationId: json['education_id'] as int,
      emailNewsletters: json['email_newsletters'] as bool,
      emailPromotions: json['email_promotions'] as bool,
      anonimized: json['anonimized'] as int,
      credit: json['credit'] as int,
      fullname: json['fullname'] as String,
    );

Map<String, dynamic> _$$OwnerResponseImplToJson(_$OwnerResponseImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'firstname': instance.firstname,
      'insertion': instance.insertion,
      'lastname': instance.lastname,
      'country': instance.country,
      'street': instance.street,
      'postal': instance.postal,
      'city': instance.city,
      'phone': instance.phone,
      'dob': instance.dob.toIso8601String(),
      'gender': instance.gender,
      'studentnumber': instance.studentnumber,
      'comments': instance.comments,
      'contribution': instance.contribution,
      'transaction': instance.transaction,
      'sbc': instance.sbc,
      'pas_created': instance.pasCreated,
      'mandaat_id': instance.mandaatId,
      'uid': instance.uid,
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'education_id': instance.educationId,
      'email_newsletters': instance.emailNewsletters,
      'email_promotions': instance.emailPromotions,
      'anonimized': instance.anonimized,
      'credit': instance.credit,
      'fullname': instance.fullname,
    };
