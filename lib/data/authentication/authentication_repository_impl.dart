import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../core/authentication/authentication_repository.dart';
import '../../core/authentication/syntaxis_user.dart';
import '../rest_client/rest_client.dart';
import 'authentication_request_dto.dart';
import 'authentication_response.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();
  final RestClient _client = RestClient(Dio());

  @override
  Future<SyntaxisUser?> login(String email, String password,
      {bool passwordIsHashed = false}) async {
    final AuthenticationRequestDto requestDto = AuthenticationRequestDto(
        email,
        passwordIsHashed
            ? password
            : sha512.convert(password.codeUnits).toString());
    try {
      final AuthenticationResponse response =
          await _client.postSignIn(requestDto);
      if (response.success) {
        await _secureStorage.write(key: 'email', value: email);
        await _secureStorage.write(
            key: 'password',
            value: passwordIsHashed
                ? password
                : sha512.convert(password.codeUnits).toString());
      }
      return response.toSyntaxisUser;
    } catch (e) {
      //   // TODO(daniel): Handle failed sign-in
      await _secureStorage.delete(key: 'email');
      await _secureStorage.delete(key: 'password');
      rethrow;
    }
  }

  @override
  Future<SyntaxisUser?> loginFromToken() async {
    if (await _secureStorage.containsKey(key: 'email') &&
        await _secureStorage.containsKey(key: 'password')) {
      final String? email = await _secureStorage.read(key: 'email');
      final String? passwordHash = await _secureStorage.read(key: 'password');
      return login(email!, passwordHash!, passwordIsHashed: true);
    }
    return null;
  }
}
