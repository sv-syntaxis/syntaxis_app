import 'package:json_annotation/json_annotation.dart';

part 'authentication_request_dto.g.dart';

@JsonSerializable()
class AuthenticationRequestDto  {
    final String email;
    final String password;

  AuthenticationRequestDto(this.email, this.password);

  /// Create a new [AuthenticationRequestDto] from json.
  factory AuthenticationRequestDto.fromJson(Map<String, dynamic> json) =>
      _$AuthenticationRequestDtoFromJson(json);


  /// Converts the response into Json.
  Map<String, dynamic> toJson() => _$AuthenticationRequestDtoToJson(this);
}
