// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'authentication_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

AuthenticationResponse _$AuthenticationResponseFromJson(
    Map<String, dynamic> json) {
  return _AuthenticationResponse.fromJson(json);
}

/// @nodoc
mixin _$AuthenticationResponse {
  bool get success => throw _privateConstructorUsedError;
  String? get generalMessage => throw _privateConstructorUsedError;
  String? get error => throw _privateConstructorUsedError;
  AuthenticationPayloadResponse get payload =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthenticationResponseCopyWith<AuthenticationResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthenticationResponseCopyWith<$Res> {
  factory $AuthenticationResponseCopyWith(AuthenticationResponse value,
          $Res Function(AuthenticationResponse) then) =
      _$AuthenticationResponseCopyWithImpl<$Res, AuthenticationResponse>;
  @useResult
  $Res call(
      {bool success,
      String? generalMessage,
      String? error,
      AuthenticationPayloadResponse payload});

  $AuthenticationPayloadResponseCopyWith<$Res> get payload;
}

/// @nodoc
class _$AuthenticationResponseCopyWithImpl<$Res,
        $Val extends AuthenticationResponse>
    implements $AuthenticationResponseCopyWith<$Res> {
  _$AuthenticationResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? success = null,
    Object? generalMessage = freezed,
    Object? error = freezed,
    Object? payload = null,
  }) {
    return _then(_value.copyWith(
      success: null == success
          ? _value.success
          : success // ignore: cast_nullable_to_non_nullable
              as bool,
      generalMessage: freezed == generalMessage
          ? _value.generalMessage
          : generalMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      error: freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
      payload: null == payload
          ? _value.payload
          : payload // ignore: cast_nullable_to_non_nullable
              as AuthenticationPayloadResponse,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $AuthenticationPayloadResponseCopyWith<$Res> get payload {
    return $AuthenticationPayloadResponseCopyWith<$Res>(_value.payload,
        (value) {
      return _then(_value.copyWith(payload: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$AuthenticationResponseImplCopyWith<$Res>
    implements $AuthenticationResponseCopyWith<$Res> {
  factory _$$AuthenticationResponseImplCopyWith(
          _$AuthenticationResponseImpl value,
          $Res Function(_$AuthenticationResponseImpl) then) =
      __$$AuthenticationResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool success,
      String? generalMessage,
      String? error,
      AuthenticationPayloadResponse payload});

  @override
  $AuthenticationPayloadResponseCopyWith<$Res> get payload;
}

/// @nodoc
class __$$AuthenticationResponseImplCopyWithImpl<$Res>
    extends _$AuthenticationResponseCopyWithImpl<$Res,
        _$AuthenticationResponseImpl>
    implements _$$AuthenticationResponseImplCopyWith<$Res> {
  __$$AuthenticationResponseImplCopyWithImpl(
      _$AuthenticationResponseImpl _value,
      $Res Function(_$AuthenticationResponseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? success = null,
    Object? generalMessage = freezed,
    Object? error = freezed,
    Object? payload = null,
  }) {
    return _then(_$AuthenticationResponseImpl(
      success: null == success
          ? _value.success
          : success // ignore: cast_nullable_to_non_nullable
              as bool,
      generalMessage: freezed == generalMessage
          ? _value.generalMessage
          : generalMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      error: freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
      payload: null == payload
          ? _value.payload
          : payload // ignore: cast_nullable_to_non_nullable
              as AuthenticationPayloadResponse,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$AuthenticationResponseImpl extends _AuthenticationResponse {
  const _$AuthenticationResponseImpl(
      {required this.success,
      required this.generalMessage,
      required this.error,
      required this.payload})
      : super._();

  factory _$AuthenticationResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$AuthenticationResponseImplFromJson(json);

  @override
  final bool success;
  @override
  final String? generalMessage;
  @override
  final String? error;
  @override
  final AuthenticationPayloadResponse payload;

  @override
  String toString() {
    return 'AuthenticationResponse(success: $success, generalMessage: $generalMessage, error: $error, payload: $payload)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthenticationResponseImpl &&
            (identical(other.success, success) || other.success == success) &&
            (identical(other.generalMessage, generalMessage) ||
                other.generalMessage == generalMessage) &&
            (identical(other.error, error) || other.error == error) &&
            (identical(other.payload, payload) || other.payload == payload));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, success, generalMessage, error, payload);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthenticationResponseImplCopyWith<_$AuthenticationResponseImpl>
      get copyWith => __$$AuthenticationResponseImplCopyWithImpl<
          _$AuthenticationResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$AuthenticationResponseImplToJson(
      this,
    );
  }
}

abstract class _AuthenticationResponse extends AuthenticationResponse {
  const factory _AuthenticationResponse(
          {required final bool success,
          required final String? generalMessage,
          required final String? error,
          required final AuthenticationPayloadResponse payload}) =
      _$AuthenticationResponseImpl;
  const _AuthenticationResponse._() : super._();

  factory _AuthenticationResponse.fromJson(Map<String, dynamic> json) =
      _$AuthenticationResponseImpl.fromJson;

  @override
  bool get success;
  @override
  String? get generalMessage;
  @override
  String? get error;
  @override
  AuthenticationPayloadResponse get payload;
  @override
  @JsonKey(ignore: true)
  _$$AuthenticationResponseImplCopyWith<_$AuthenticationResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}

AuthenticationPayloadResponse _$AuthenticationPayloadResponseFromJson(
    Map<String, dynamic> json) {
  return _AuthenticationPayloadResponse.fromJson(json);
}

/// @nodoc
mixin _$AuthenticationPayloadResponse {
  @JsonKey(name: 'user_id')
  int get userId => throw _privateConstructorUsedError;
  String get token => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  DateTime get createdAt => throw _privateConstructorUsedError;
  OwnerResponse get owner => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthenticationPayloadResponseCopyWith<AuthenticationPayloadResponse>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthenticationPayloadResponseCopyWith<$Res> {
  factory $AuthenticationPayloadResponseCopyWith(
          AuthenticationPayloadResponse value,
          $Res Function(AuthenticationPayloadResponse) then) =
      _$AuthenticationPayloadResponseCopyWithImpl<$Res,
          AuthenticationPayloadResponse>;
  @useResult
  $Res call(
      {@JsonKey(name: 'user_id') int userId,
      String token,
      @JsonKey(name: 'created_at') DateTime createdAt,
      OwnerResponse owner});

  $OwnerResponseCopyWith<$Res> get owner;
}

/// @nodoc
class _$AuthenticationPayloadResponseCopyWithImpl<$Res,
        $Val extends AuthenticationPayloadResponse>
    implements $AuthenticationPayloadResponseCopyWith<$Res> {
  _$AuthenticationPayloadResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userId = null,
    Object? token = null,
    Object? createdAt = null,
    Object? owner = null,
  }) {
    return _then(_value.copyWith(
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      owner: null == owner
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as OwnerResponse,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $OwnerResponseCopyWith<$Res> get owner {
    return $OwnerResponseCopyWith<$Res>(_value.owner, (value) {
      return _then(_value.copyWith(owner: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$AuthenticationPayloadResponseImplCopyWith<$Res>
    implements $AuthenticationPayloadResponseCopyWith<$Res> {
  factory _$$AuthenticationPayloadResponseImplCopyWith(
          _$AuthenticationPayloadResponseImpl value,
          $Res Function(_$AuthenticationPayloadResponseImpl) then) =
      __$$AuthenticationPayloadResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'user_id') int userId,
      String token,
      @JsonKey(name: 'created_at') DateTime createdAt,
      OwnerResponse owner});

  @override
  $OwnerResponseCopyWith<$Res> get owner;
}

/// @nodoc
class __$$AuthenticationPayloadResponseImplCopyWithImpl<$Res>
    extends _$AuthenticationPayloadResponseCopyWithImpl<$Res,
        _$AuthenticationPayloadResponseImpl>
    implements _$$AuthenticationPayloadResponseImplCopyWith<$Res> {
  __$$AuthenticationPayloadResponseImplCopyWithImpl(
      _$AuthenticationPayloadResponseImpl _value,
      $Res Function(_$AuthenticationPayloadResponseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userId = null,
    Object? token = null,
    Object? createdAt = null,
    Object? owner = null,
  }) {
    return _then(_$AuthenticationPayloadResponseImpl(
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      owner: null == owner
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as OwnerResponse,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$AuthenticationPayloadResponseImpl
    implements _AuthenticationPayloadResponse {
  const _$AuthenticationPayloadResponseImpl(
      {@JsonKey(name: 'user_id') required this.userId,
      required this.token,
      @JsonKey(name: 'created_at') required this.createdAt,
      required this.owner});

  factory _$AuthenticationPayloadResponseImpl.fromJson(
          Map<String, dynamic> json) =>
      _$$AuthenticationPayloadResponseImplFromJson(json);

  @override
  @JsonKey(name: 'user_id')
  final int userId;
  @override
  final String token;
  @override
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @override
  final OwnerResponse owner;

  @override
  String toString() {
    return 'AuthenticationPayloadResponse(userId: $userId, token: $token, createdAt: $createdAt, owner: $owner)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthenticationPayloadResponseImpl &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.owner, owner) || other.owner == owner));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, userId, token, createdAt, owner);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthenticationPayloadResponseImplCopyWith<
          _$AuthenticationPayloadResponseImpl>
      get copyWith => __$$AuthenticationPayloadResponseImplCopyWithImpl<
          _$AuthenticationPayloadResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$AuthenticationPayloadResponseImplToJson(
      this,
    );
  }
}

abstract class _AuthenticationPayloadResponse
    implements AuthenticationPayloadResponse {
  const factory _AuthenticationPayloadResponse(
          {@JsonKey(name: 'user_id') required final int userId,
          required final String token,
          @JsonKey(name: 'created_at') required final DateTime createdAt,
          required final OwnerResponse owner}) =
      _$AuthenticationPayloadResponseImpl;

  factory _AuthenticationPayloadResponse.fromJson(Map<String, dynamic> json) =
      _$AuthenticationPayloadResponseImpl.fromJson;

  @override
  @JsonKey(name: 'user_id')
  int get userId;
  @override
  String get token;
  @override
  @JsonKey(name: 'created_at')
  DateTime get createdAt;
  @override
  OwnerResponse get owner;
  @override
  @JsonKey(ignore: true)
  _$$AuthenticationPayloadResponseImplCopyWith<
          _$AuthenticationPayloadResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}

OwnerResponse _$OwnerResponseFromJson(Map<String, dynamic> json) {
  return _OwnerResponse.fromJson(json);
}

/// @nodoc
mixin _$OwnerResponse {
  int get id => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get firstname => throw _privateConstructorUsedError;
  String get insertion => throw _privateConstructorUsedError;
  String get lastname => throw _privateConstructorUsedError;
  String get country => throw _privateConstructorUsedError;
  String get street => throw _privateConstructorUsedError;
  String get postal => throw _privateConstructorUsedError;
  String get city => throw _privateConstructorUsedError;
  String get phone => throw _privateConstructorUsedError;
  DateTime get dob => throw _privateConstructorUsedError;
  String get gender => throw _privateConstructorUsedError;
  String get studentnumber => throw _privateConstructorUsedError;
  String? get comments => throw _privateConstructorUsedError;
  bool get contribution => throw _privateConstructorUsedError;
  bool get transaction => throw _privateConstructorUsedError;
  bool get sbc => throw _privateConstructorUsedError;
  @JsonKey(name: 'pas_created')
  bool get pasCreated => throw _privateConstructorUsedError;
  @JsonKey(name: 'mandaat_id')
  int get mandaatId => throw _privateConstructorUsedError;
  String get uid => throw _privateConstructorUsedError;
  @JsonKey(name: 'deleted_at')
  DateTime? get deletedAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  DateTime get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  DateTime? get updatedAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'education_id')
  int get educationId => throw _privateConstructorUsedError;
  @JsonKey(name: 'email_newsletters')
  bool get emailNewsletters => throw _privateConstructorUsedError;
  @JsonKey(name: 'email_promotions')
  bool get emailPromotions => throw _privateConstructorUsedError;
  int get anonimized => throw _privateConstructorUsedError;
  int get credit => throw _privateConstructorUsedError;
  String get fullname => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $OwnerResponseCopyWith<OwnerResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OwnerResponseCopyWith<$Res> {
  factory $OwnerResponseCopyWith(
          OwnerResponse value, $Res Function(OwnerResponse) then) =
      _$OwnerResponseCopyWithImpl<$Res, OwnerResponse>;
  @useResult
  $Res call(
      {int id,
      String email,
      String firstname,
      String insertion,
      String lastname,
      String country,
      String street,
      String postal,
      String city,
      String phone,
      DateTime dob,
      String gender,
      String studentnumber,
      String? comments,
      bool contribution,
      bool transaction,
      bool sbc,
      @JsonKey(name: 'pas_created') bool pasCreated,
      @JsonKey(name: 'mandaat_id') int mandaatId,
      String uid,
      @JsonKey(name: 'deleted_at') DateTime? deletedAt,
      @JsonKey(name: 'created_at') DateTime createdAt,
      @JsonKey(name: 'updated_at') DateTime? updatedAt,
      @JsonKey(name: 'education_id') int educationId,
      @JsonKey(name: 'email_newsletters') bool emailNewsletters,
      @JsonKey(name: 'email_promotions') bool emailPromotions,
      int anonimized,
      int credit,
      String fullname});
}

/// @nodoc
class _$OwnerResponseCopyWithImpl<$Res, $Val extends OwnerResponse>
    implements $OwnerResponseCopyWith<$Res> {
  _$OwnerResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? email = null,
    Object? firstname = null,
    Object? insertion = null,
    Object? lastname = null,
    Object? country = null,
    Object? street = null,
    Object? postal = null,
    Object? city = null,
    Object? phone = null,
    Object? dob = null,
    Object? gender = null,
    Object? studentnumber = null,
    Object? comments = freezed,
    Object? contribution = null,
    Object? transaction = null,
    Object? sbc = null,
    Object? pasCreated = null,
    Object? mandaatId = null,
    Object? uid = null,
    Object? deletedAt = freezed,
    Object? createdAt = null,
    Object? updatedAt = freezed,
    Object? educationId = null,
    Object? emailNewsletters = null,
    Object? emailPromotions = null,
    Object? anonimized = null,
    Object? credit = null,
    Object? fullname = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      firstname: null == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String,
      insertion: null == insertion
          ? _value.insertion
          : insertion // ignore: cast_nullable_to_non_nullable
              as String,
      lastname: null == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String,
      country: null == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      street: null == street
          ? _value.street
          : street // ignore: cast_nullable_to_non_nullable
              as String,
      postal: null == postal
          ? _value.postal
          : postal // ignore: cast_nullable_to_non_nullable
              as String,
      city: null == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      dob: null == dob
          ? _value.dob
          : dob // ignore: cast_nullable_to_non_nullable
              as DateTime,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      studentnumber: null == studentnumber
          ? _value.studentnumber
          : studentnumber // ignore: cast_nullable_to_non_nullable
              as String,
      comments: freezed == comments
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as String?,
      contribution: null == contribution
          ? _value.contribution
          : contribution // ignore: cast_nullable_to_non_nullable
              as bool,
      transaction: null == transaction
          ? _value.transaction
          : transaction // ignore: cast_nullable_to_non_nullable
              as bool,
      sbc: null == sbc
          ? _value.sbc
          : sbc // ignore: cast_nullable_to_non_nullable
              as bool,
      pasCreated: null == pasCreated
          ? _value.pasCreated
          : pasCreated // ignore: cast_nullable_to_non_nullable
              as bool,
      mandaatId: null == mandaatId
          ? _value.mandaatId
          : mandaatId // ignore: cast_nullable_to_non_nullable
              as int,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      deletedAt: freezed == deletedAt
          ? _value.deletedAt
          : deletedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      educationId: null == educationId
          ? _value.educationId
          : educationId // ignore: cast_nullable_to_non_nullable
              as int,
      emailNewsletters: null == emailNewsletters
          ? _value.emailNewsletters
          : emailNewsletters // ignore: cast_nullable_to_non_nullable
              as bool,
      emailPromotions: null == emailPromotions
          ? _value.emailPromotions
          : emailPromotions // ignore: cast_nullable_to_non_nullable
              as bool,
      anonimized: null == anonimized
          ? _value.anonimized
          : anonimized // ignore: cast_nullable_to_non_nullable
              as int,
      credit: null == credit
          ? _value.credit
          : credit // ignore: cast_nullable_to_non_nullable
              as int,
      fullname: null == fullname
          ? _value.fullname
          : fullname // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$OwnerResponseImplCopyWith<$Res>
    implements $OwnerResponseCopyWith<$Res> {
  factory _$$OwnerResponseImplCopyWith(
          _$OwnerResponseImpl value, $Res Function(_$OwnerResponseImpl) then) =
      __$$OwnerResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String email,
      String firstname,
      String insertion,
      String lastname,
      String country,
      String street,
      String postal,
      String city,
      String phone,
      DateTime dob,
      String gender,
      String studentnumber,
      String? comments,
      bool contribution,
      bool transaction,
      bool sbc,
      @JsonKey(name: 'pas_created') bool pasCreated,
      @JsonKey(name: 'mandaat_id') int mandaatId,
      String uid,
      @JsonKey(name: 'deleted_at') DateTime? deletedAt,
      @JsonKey(name: 'created_at') DateTime createdAt,
      @JsonKey(name: 'updated_at') DateTime? updatedAt,
      @JsonKey(name: 'education_id') int educationId,
      @JsonKey(name: 'email_newsletters') bool emailNewsletters,
      @JsonKey(name: 'email_promotions') bool emailPromotions,
      int anonimized,
      int credit,
      String fullname});
}

/// @nodoc
class __$$OwnerResponseImplCopyWithImpl<$Res>
    extends _$OwnerResponseCopyWithImpl<$Res, _$OwnerResponseImpl>
    implements _$$OwnerResponseImplCopyWith<$Res> {
  __$$OwnerResponseImplCopyWithImpl(
      _$OwnerResponseImpl _value, $Res Function(_$OwnerResponseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? email = null,
    Object? firstname = null,
    Object? insertion = null,
    Object? lastname = null,
    Object? country = null,
    Object? street = null,
    Object? postal = null,
    Object? city = null,
    Object? phone = null,
    Object? dob = null,
    Object? gender = null,
    Object? studentnumber = null,
    Object? comments = freezed,
    Object? contribution = null,
    Object? transaction = null,
    Object? sbc = null,
    Object? pasCreated = null,
    Object? mandaatId = null,
    Object? uid = null,
    Object? deletedAt = freezed,
    Object? createdAt = null,
    Object? updatedAt = freezed,
    Object? educationId = null,
    Object? emailNewsletters = null,
    Object? emailPromotions = null,
    Object? anonimized = null,
    Object? credit = null,
    Object? fullname = null,
  }) {
    return _then(_$OwnerResponseImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      firstname: null == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String,
      insertion: null == insertion
          ? _value.insertion
          : insertion // ignore: cast_nullable_to_non_nullable
              as String,
      lastname: null == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String,
      country: null == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      street: null == street
          ? _value.street
          : street // ignore: cast_nullable_to_non_nullable
              as String,
      postal: null == postal
          ? _value.postal
          : postal // ignore: cast_nullable_to_non_nullable
              as String,
      city: null == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      dob: null == dob
          ? _value.dob
          : dob // ignore: cast_nullable_to_non_nullable
              as DateTime,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      studentnumber: null == studentnumber
          ? _value.studentnumber
          : studentnumber // ignore: cast_nullable_to_non_nullable
              as String,
      comments: freezed == comments
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as String?,
      contribution: null == contribution
          ? _value.contribution
          : contribution // ignore: cast_nullable_to_non_nullable
              as bool,
      transaction: null == transaction
          ? _value.transaction
          : transaction // ignore: cast_nullable_to_non_nullable
              as bool,
      sbc: null == sbc
          ? _value.sbc
          : sbc // ignore: cast_nullable_to_non_nullable
              as bool,
      pasCreated: null == pasCreated
          ? _value.pasCreated
          : pasCreated // ignore: cast_nullable_to_non_nullable
              as bool,
      mandaatId: null == mandaatId
          ? _value.mandaatId
          : mandaatId // ignore: cast_nullable_to_non_nullable
              as int,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      deletedAt: freezed == deletedAt
          ? _value.deletedAt
          : deletedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      educationId: null == educationId
          ? _value.educationId
          : educationId // ignore: cast_nullable_to_non_nullable
              as int,
      emailNewsletters: null == emailNewsletters
          ? _value.emailNewsletters
          : emailNewsletters // ignore: cast_nullable_to_non_nullable
              as bool,
      emailPromotions: null == emailPromotions
          ? _value.emailPromotions
          : emailPromotions // ignore: cast_nullable_to_non_nullable
              as bool,
      anonimized: null == anonimized
          ? _value.anonimized
          : anonimized // ignore: cast_nullable_to_non_nullable
              as int,
      credit: null == credit
          ? _value.credit
          : credit // ignore: cast_nullable_to_non_nullable
              as int,
      fullname: null == fullname
          ? _value.fullname
          : fullname // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$OwnerResponseImpl extends _OwnerResponse {
  const _$OwnerResponseImpl(
      {required this.id,
      required this.email,
      required this.firstname,
      required this.insertion,
      required this.lastname,
      required this.country,
      required this.street,
      required this.postal,
      required this.city,
      required this.phone,
      required this.dob,
      required this.gender,
      required this.studentnumber,
      required this.comments,
      required this.contribution,
      required this.transaction,
      required this.sbc,
      @JsonKey(name: 'pas_created') required this.pasCreated,
      @JsonKey(name: 'mandaat_id') required this.mandaatId,
      required this.uid,
      @JsonKey(name: 'deleted_at') required this.deletedAt,
      @JsonKey(name: 'created_at') required this.createdAt,
      @JsonKey(name: 'updated_at') required this.updatedAt,
      @JsonKey(name: 'education_id') required this.educationId,
      @JsonKey(name: 'email_newsletters') required this.emailNewsletters,
      @JsonKey(name: 'email_promotions') required this.emailPromotions,
      required this.anonimized,
      required this.credit,
      required this.fullname})
      : super._();

  factory _$OwnerResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$OwnerResponseImplFromJson(json);

  @override
  final int id;
  @override
  final String email;
  @override
  final String firstname;
  @override
  final String insertion;
  @override
  final String lastname;
  @override
  final String country;
  @override
  final String street;
  @override
  final String postal;
  @override
  final String city;
  @override
  final String phone;
  @override
  final DateTime dob;
  @override
  final String gender;
  @override
  final String studentnumber;
  @override
  final String? comments;
  @override
  final bool contribution;
  @override
  final bool transaction;
  @override
  final bool sbc;
  @override
  @JsonKey(name: 'pas_created')
  final bool pasCreated;
  @override
  @JsonKey(name: 'mandaat_id')
  final int mandaatId;
  @override
  final String uid;
  @override
  @JsonKey(name: 'deleted_at')
  final DateTime? deletedAt;
  @override
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final DateTime? updatedAt;
  @override
  @JsonKey(name: 'education_id')
  final int educationId;
  @override
  @JsonKey(name: 'email_newsletters')
  final bool emailNewsletters;
  @override
  @JsonKey(name: 'email_promotions')
  final bool emailPromotions;
  @override
  final int anonimized;
  @override
  final int credit;
  @override
  final String fullname;

  @override
  String toString() {
    return 'OwnerResponse(id: $id, email: $email, firstname: $firstname, insertion: $insertion, lastname: $lastname, country: $country, street: $street, postal: $postal, city: $city, phone: $phone, dob: $dob, gender: $gender, studentnumber: $studentnumber, comments: $comments, contribution: $contribution, transaction: $transaction, sbc: $sbc, pasCreated: $pasCreated, mandaatId: $mandaatId, uid: $uid, deletedAt: $deletedAt, createdAt: $createdAt, updatedAt: $updatedAt, educationId: $educationId, emailNewsletters: $emailNewsletters, emailPromotions: $emailPromotions, anonimized: $anonimized, credit: $credit, fullname: $fullname)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OwnerResponseImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.firstname, firstname) ||
                other.firstname == firstname) &&
            (identical(other.insertion, insertion) ||
                other.insertion == insertion) &&
            (identical(other.lastname, lastname) ||
                other.lastname == lastname) &&
            (identical(other.country, country) || other.country == country) &&
            (identical(other.street, street) || other.street == street) &&
            (identical(other.postal, postal) || other.postal == postal) &&
            (identical(other.city, city) || other.city == city) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.dob, dob) || other.dob == dob) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.studentnumber, studentnumber) ||
                other.studentnumber == studentnumber) &&
            (identical(other.comments, comments) ||
                other.comments == comments) &&
            (identical(other.contribution, contribution) ||
                other.contribution == contribution) &&
            (identical(other.transaction, transaction) ||
                other.transaction == transaction) &&
            (identical(other.sbc, sbc) || other.sbc == sbc) &&
            (identical(other.pasCreated, pasCreated) ||
                other.pasCreated == pasCreated) &&
            (identical(other.mandaatId, mandaatId) ||
                other.mandaatId == mandaatId) &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.deletedAt, deletedAt) ||
                other.deletedAt == deletedAt) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            (identical(other.educationId, educationId) ||
                other.educationId == educationId) &&
            (identical(other.emailNewsletters, emailNewsletters) ||
                other.emailNewsletters == emailNewsletters) &&
            (identical(other.emailPromotions, emailPromotions) ||
                other.emailPromotions == emailPromotions) &&
            (identical(other.anonimized, anonimized) ||
                other.anonimized == anonimized) &&
            (identical(other.credit, credit) || other.credit == credit) &&
            (identical(other.fullname, fullname) ||
                other.fullname == fullname));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        email,
        firstname,
        insertion,
        lastname,
        country,
        street,
        postal,
        city,
        phone,
        dob,
        gender,
        studentnumber,
        comments,
        contribution,
        transaction,
        sbc,
        pasCreated,
        mandaatId,
        uid,
        deletedAt,
        createdAt,
        updatedAt,
        educationId,
        emailNewsletters,
        emailPromotions,
        anonimized,
        credit,
        fullname
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$OwnerResponseImplCopyWith<_$OwnerResponseImpl> get copyWith =>
      __$$OwnerResponseImplCopyWithImpl<_$OwnerResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$OwnerResponseImplToJson(
      this,
    );
  }
}

abstract class _OwnerResponse extends OwnerResponse {
  const factory _OwnerResponse(
      {required final int id,
      required final String email,
      required final String firstname,
      required final String insertion,
      required final String lastname,
      required final String country,
      required final String street,
      required final String postal,
      required final String city,
      required final String phone,
      required final DateTime dob,
      required final String gender,
      required final String studentnumber,
      required final String? comments,
      required final bool contribution,
      required final bool transaction,
      required final bool sbc,
      @JsonKey(name: 'pas_created') required final bool pasCreated,
      @JsonKey(name: 'mandaat_id') required final int mandaatId,
      required final String uid,
      @JsonKey(name: 'deleted_at') required final DateTime? deletedAt,
      @JsonKey(name: 'created_at') required final DateTime createdAt,
      @JsonKey(name: 'updated_at') required final DateTime? updatedAt,
      @JsonKey(name: 'education_id') required final int educationId,
      @JsonKey(name: 'email_newsletters') required final bool emailNewsletters,
      @JsonKey(name: 'email_promotions') required final bool emailPromotions,
      required final int anonimized,
      required final int credit,
      required final String fullname}) = _$OwnerResponseImpl;
  const _OwnerResponse._() : super._();

  factory _OwnerResponse.fromJson(Map<String, dynamic> json) =
      _$OwnerResponseImpl.fromJson;

  @override
  int get id;
  @override
  String get email;
  @override
  String get firstname;
  @override
  String get insertion;
  @override
  String get lastname;
  @override
  String get country;
  @override
  String get street;
  @override
  String get postal;
  @override
  String get city;
  @override
  String get phone;
  @override
  DateTime get dob;
  @override
  String get gender;
  @override
  String get studentnumber;
  @override
  String? get comments;
  @override
  bool get contribution;
  @override
  bool get transaction;
  @override
  bool get sbc;
  @override
  @JsonKey(name: 'pas_created')
  bool get pasCreated;
  @override
  @JsonKey(name: 'mandaat_id')
  int get mandaatId;
  @override
  String get uid;
  @override
  @JsonKey(name: 'deleted_at')
  DateTime? get deletedAt;
  @override
  @JsonKey(name: 'created_at')
  DateTime get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  DateTime? get updatedAt;
  @override
  @JsonKey(name: 'education_id')
  int get educationId;
  @override
  @JsonKey(name: 'email_newsletters')
  bool get emailNewsletters;
  @override
  @JsonKey(name: 'email_promotions')
  bool get emailPromotions;
  @override
  int get anonimized;
  @override
  int get credit;
  @override
  String get fullname;
  @override
  @JsonKey(ignore: true)
  _$$OwnerResponseImplCopyWith<_$OwnerResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
