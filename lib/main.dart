import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/authentication/authentication_service.dart';
import 'data/authentication/authentication_repository_impl.dart';
import 'presentation/app/app.dart';
import 'presentation/authentication/cubit/auth_cubit.dart';

void main() {
  runApp(
    BlocProvider<AuthCubit>(
      create: (BuildContext context) => AuthCubit(
          authService: AuthenticationService(
              authenticationRepository: AuthenticationRepositoryImpl())..initialize()),
      child: const App(),
    ),
  );
}
